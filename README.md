# Cloud basics

Simple application displaying computer ressources. Goals are to implements basics steps of DevOps usages.

Template from [kannurien](https://github.com/khannurien/i-want-typescript).

## Prerequisites

Running on Node 16.

## Usage

Install development dependencies:

```
npm install
```

Format then lint `src/*.ts` by making in-place fixes:

```
npm run fix
```

Run unit test suites:

```
npm run test
```

View coverage of unit tests:

```
npm run test:coverage
```

Build `src/*.ts` files into `dist/*.js` files:

```
npm run build
```

Serve `dist/index.js` using `node` (for production):

```
npm run start
```

Monitor file changes and serve `src/index.ts` using `nodemon` with `ts-node` (for development):

```
npm run watch
```

Run all 
```
npm run go
```

Run TS execution
```
npm run exec
```

Run js compiled files
```
node src/index.js
```
