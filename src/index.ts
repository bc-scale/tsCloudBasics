import * as si from 'systeminformation';
import * as http from 'http';

export interface ISystemInformation {
  cpu: si.Systeminformation.CpuData;
  system: si.Systeminformation.SystemData;
  mem: si.Systeminformation.MemData;
  os: si.Systeminformation.OsData;
  currentLoad: si.Systeminformation.CurrentLoadData;
  processes: si.Systeminformation.ProcessesData;
  diskLayout: si.Systeminformation.DiskLayoutData[];
  networkInterfaces: si.Systeminformation.NetworkInterfacesData[];
}

const funcs = [
  si.cpu,
  si.system,
  si.mem,
  si.osInfo,
  si.currentLoad,
  si.processes,
  si.diskLayout,
  si.networkInterfaces,
];

export async function getResult(foncs: (() => any)[]): Promise<any[]> {
  const promises = [];
  let res = [];

  foncs.forEach((f) => promises.push(f()));

  res = await Promise.all(promises);
  return res;
}

export function format(input: any) {
  const res: ISystemInformation = {
    cpu: input[0],
    system: input[1],
    mem: input[2],
    os: input[3],
    currentLoad: input[4],
    processes: input[5],
    diskLayout: input[6],
    networkInterfaces: input[7],
  };
  return res;
}

export function serve() {
  http
    .createServer(async (req, res) => {
      const rawRes = await getResult(funcs);
      const iSystemInformation = format(rawRes);

      if (req.url === '/api/v1/sysinfo') {
        res.writeHead(200, { 'Content-type': 'application/json' });
        res.write(JSON.stringify(iSystemInformation));
        res.end();
      } else {
        res.statusCode = 404;
        res.end();
      }
    })
    .listen(process.env.PORT || 8080);
  console.log('server ready : http://localhost:'+(process.env.PORT || 8080));
}

serve();
